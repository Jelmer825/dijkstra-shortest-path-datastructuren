package program;

import com.sun.org.apache.xpath.internal.operations.Bool;
import javafx.util.Pair;

import java.util.*;

public class DPQ {

    private final LinkedList<Reis> reisList = new LinkedList<>();

    public void setWeights(LinkedList<Stap> possibleDestinations){
        for (Stap stap : possibleDestinations)
        {
            Random random = new Random();
            stap.setWeight(random.nextInt(10)+1);
        }
    }

    public void dijkstra(Stap init, int size){
        LinkedList<Pair<Stap, Double>> lowestCostPerPath = new LinkedList<>();

        PriorityQueue<Reis> frontier = new PriorityQueue<Reis>();
        LinkedList<Reis> explored = new LinkedList<Reis>();

        // Adds first
        Reis initial = new Reis(init, init, 0.0, 0.0);
        frontier.add(initial);
        explored.add(initial);

        do {

            Reis lowest = frontier.peek();

            // Loop through every edge from the current node out of the PQ
            for (Pair<Stap, Double> edge : lowest.getReisPair().getValue().getEdges()) {
                // Look if step has not been made
                Reis reis = new Reis(lowest.getReisPair().getValue(), edge.getKey(), edge.getValue(), lowest.getTotalCost());
                if (!explored.contains(reis) && !this.checkTravelDifference(reis, explored)) {
                    // Look if next step is still the last one
                    if (lowest.getTotalCost() == 0 || edge.getValue() < lowest.getTotalCost()) {
                        // Look if 'reis' is not in frontier
                        frontier.remove(initial);
                        if (!frontier.contains(reis)) {
                            // Make 'travel' to the next node
                            frontier.add(reis);
                            explored.add(reis);
                        }
                    }
                }
            }
            if (lowest != initial) {
                Pair<Stap, Double> pair = new Pair<>(lowest.getReisPair().getValue(), lowest.getTotalCost());
                lowestCostPerPath.add(pair);
                frontier.remove(lowest);
            }

        } while (lowestCostPerPath.size() != size+1);
        for(Pair<Stap, Double> pair: lowestCostPerPath){
            System.out.println(pair.getKey().getNode() + "   " + pair.getValue());
        }
    }

    public Boolean checkTravelDifference(Reis r1, LinkedList<Reis> explored){
        for(Reis r2: explored){
            if (r1.getReisPair().getKey() == r2.getReisPair().getValue() && r1.getReisPair().getValue() == r2.getReisPair().getKey()){
                return true;
            }
        }
        return false;
    }
    public LinkedList<Reis> getReisList() {
        return reisList;
    }
}
