package program;

public class Treinrit extends Stap {

    public Treinrit(String node) {
        super(node);
    }

    @Override
    void setWeight(int weight) {
        this.weight = weight;
    }
}
