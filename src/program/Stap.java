package program;

import javafx.util.Pair;

import java.util.LinkedList;
import java.util.List;

public abstract class Stap implements Comparable<Stap>{

    private final String node;
    protected double weight;

    private final LinkedList<Pair<Stap, Double>> edges = new LinkedList<>();

    public Stap(String node){
        this.node = node;
    }

    public String getNode() {
        return node;
    }

    public double getWeight() {
        return weight;
    }

    public LinkedList<Pair<Stap, Double>> getEdges() {
        return edges;
    }

    protected void makeEdge(Stap stap){
        Double edgeWeight = stap.getWeight() + this.weight;
        Pair<Stap, Double> pair = new Pair<>(stap, edgeWeight);
        this.edges.add(pair);
    }

    @Override
    public int compareTo(Stap another) {
        if (this.weight < another.weight){
            return -1;
        }
        else if (this.weight > another.weight){
            return 1;
        }
        return 0;
    }

    abstract void setWeight(int weight);
}
