package program;

import javafx.util.Pair;

import java.util.LinkedList;

public class Main {
    public static void main(String[] args) {
        // Initialise program
        LinkedList<Stap> destinations = new LinkedList<Stap>();
        Vlucht amsterdam = new Vlucht("Amsterdam", 1);
        Vlucht brussel = new Vlucht("Brussel", 2);
        Vlucht praque = new Vlucht("Prague", 5);
        Vlucht sidney = new Vlucht("Sidney", 3);
        Vlucht brasilia = new Vlucht("Brasilia", 8);

        destinations.add(amsterdam);
        destinations.add(brussel);
        destinations.add(praque);
        destinations.add(sidney);
        destinations.add(brasilia);

        DPQ dpq1 = new DPQ();
        dpq1.setWeights(destinations);

        // Initialise edges (Dit heb ik op deze manier gedaan zodat het niet randomized is)
        amsterdam.makeEdge(brasilia);
        amsterdam.makeEdge(brussel);
        amsterdam.makeEdge(praque);

        brasilia.makeEdge(amsterdam);
        brasilia.makeEdge(sidney);

        sidney.makeEdge(brasilia);
        sidney.makeEdge(praque);
        sidney.makeEdge(brussel);

        praque.makeEdge(sidney);
        praque.makeEdge(amsterdam);
        praque.makeEdge(brussel);

        brussel.makeEdge(amsterdam);
        brussel.makeEdge(praque);
        brussel.makeEdge(sidney);

        System.out.println("Start program: ");
        System.out.println(amsterdam.getEdges().get(1).getKey().getNode() + " "+ amsterdam.getEdges().get(1).getValue());

        // Init
        dpq1.dijkstra(brasilia, destinations.size());
    }
}
