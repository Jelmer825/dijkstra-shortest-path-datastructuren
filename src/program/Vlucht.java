package program;

public class Vlucht extends Stap{

    double baggageLoss;

    public Vlucht(String node, double baggageLoss) {
        super(node);
        this.baggageLoss = baggageLoss;
    }

    @Override
    void setWeight(int weight) {
        this.weight = Math.round(weight*this.baggageLoss);
    }
}
