package program;

import javafx.util.Pair;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Random;

public class Reis implements Comparable<Reis> {

    private double totalCost;
    private Pair<Stap, Stap> reisPair;

    public Reis(Stap p1, Stap p2, double stepCost, double lastCost){
        reisPair = new Pair<Stap, Stap>(p1, p2);
        this.totalCost = stepCost + lastCost;
    }

    public double getTotalCost() {
        return totalCost;
    }

    public Pair<Stap, Stap> getReisPair() {
        return reisPair;
    }

    @Override
    public int compareTo(Reis another) {
        if (this.totalCost < another.totalCost){
            return -1;
        }
        else if (this.totalCost > another.totalCost){
            return 1;
        }
        return 0;
    }
}
